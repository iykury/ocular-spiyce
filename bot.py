#!/usr/bin/env python3
import discord, os, random, re, time, asyncio, string, sys

#Make sure the current working directory is the correct one
file_path = os.path.abspath(__file__)
folder = os.path.dirname(file_path)
os.chdir(folder)

TIME_FORMAT = "%F %T"

if not os.path.exists("logs"):
	os.mkdir("logs")

logfile = "logs/{}.log".format(time.strftime("%F"))
num = 1
while os.path.exists(logfile):
	logfile = "logs/{}-{}.log".format(time.strftime("%F"), num)
	num += 1

def log(msg, debug=False):
	if debug:
		if debugMode:
			fullMsg = "[{}] [DEBUG] {}".format(time.strftime(TIME_FORMAT), msg)
		else:
			return
	else:
		fullMsg = "[{}] {}".format(time.strftime(TIME_FORMAT), msg)
	print(fullMsg)
	with open(logfile, "a") as f:
		f.write(fullMsg + "\n")

if len(sys.argv) > 1 and sys.argv[1] == "debug":
	debugMode = True
	log("Starting ocular spiyce in debug mode...")
else:
	debugMode = False
	log("Starting ocular spiyce...")

client = discord.Client()

async def send(channel, msg, files=None):
	try:
		log_message(await channel.send(msg, files=files))
	except Exception as e:
		log("Message failed to send: " + str(e))

def log_message(message):
	if message.channel.type == discord.ChannelType.private:
		recipient = message.channel.recipient
		channel = f"@{recipient.name}#{recipient.discriminator}"
	else:
		channel = f"{message.guild.name}#{message.channel.name}"
	
	log_msg = "{}: <{}#{}> {}".format(channel, message.author.name, message.author.discriminator, message.content)
	if not(message.attachments is None or message.attachments == []):
		urls = []
		for f in message.attachments:
			urls.append(f.url)
		log_msg += " [{}]".format(", ".join(urls))
	log(log_msg)
	return log_msg

#Get token from file
token = None
try:
	with open("token") as f:
		token = f.read().strip("\n")
except:
	log("Couldn't get token")

#Get pronoun database from file
pronoun_db = {}
if os.path.exists("pronoun_db"):
	with open("pronoun_db") as f:
		for line in f.readlines():
			key, pns = line.strip("\n").split("=")
			pronoun_db[int(key)] = [pn.split("/") for pn in pns.split(",")]

@client.event
async def on_ready():
	global channel, mod_role
	log("Logged in as")
	log(client.user.name)
	log(client.user.id)
	log("------")

@client.event
async def on_message(message):
	#Bot won't reply to itself or other bots
	if message.author.bot or message.author == client.user:
		return
	
	#Log messages
	log_message(message)

	parts = message.content.split(" ")
	command = None
	if parts[0].startswith("?"):
		command = parts[0][1:]
	
	#hello command
	if command == "hello":
		await send(message.channel, f"hello, {message.author.mention}! :D")

	#hug command
	elif command == "hug":
		if len(message.mentions) == 0:
			await send(message.channel, pronounize(f"**{message.author.name}** hugs $themself.", message.author.id))
		else:
			hugees = ", ".join([i.name for i in message.mentions])
			await send(message.channel, f"**{message.author.name}** hugs **{hugees}**.")

	#pronoun command
	elif command in ["pn", "pronoun"]:
		await pronoun_command(message, parts)

	#pronounize command
	elif command == "pronounize":
		await send(message.channel, pronounize(" ".join(parts[1:]), message.author.id))
	
	#say command
	elif command == "say":
		if message.author.id != 201874260787068928:
			await send(message.channel, "error: only iykury can use this command.")
			return

		channel_name = parts[1]
		msg = " ".join(parts[2:])
		await say(channel_name, msg, message)
	
	#spam command
	elif command == "spam":
		if message.author.id != 201874260787068928:
			await send(message.channel, "error: only iykury can use this command.")
			return

		try:
			count = int(parts[1])
		except ValueError:
			await send(message.channel, f"error: invalid number \"{parts[1]}\".")
			return

		if count < 1:
			await send(message.channel, f"error: invalid number \"{parts[1]}\".")

		channel_name = parts[2]
		msg = " ".join(parts[3:])
		await say(channel_name, msg, message, count)
	
	#excel command
	elif command == "excel":
		inputStr = " ".join(parts[1:]).lower()
		inputList = []
		
		#0 = letter, 1 = number, 2 = other  
		prev = None
		curr = None
		
		currWord = ""
		for index in range(len(inputStr)):
			char = inputStr[index]

			if char in string.ascii_lowercase:
				curr = 0
			elif char in string.digits:
				curr = 1
			else:
				curr = 2

			if prev == curr or prev is None:
				currWord += char
			else:
				inputList.append(currWord)
				currWord = char
			prev = curr

		inputList.append(currWord)

		outStr = ""
		for ye in inputList:
			alpha = string.ascii_lowercase

			if ye[0] in string.digits:
				ye = int(ye) + 1
				out = ''
				while ye > 0:
					ye, rem = divmod(ye, 26)
					stolen = bool(out and select < 0)
					select = rem - 1 - stolen
					if stolen and ye == 0 > select:
						break
					out += alpha[select]
				outStr += out[::-1]
			elif ye[0] in alpha:
				i = 1
				out = 0
				for c in ye[::-1].lower():
					val = alpha.index(c) + 1
					out += i * val
					i *= 26
				outStr += str(out - 1)
			else:
				outStr += ye

		await send(message.channel, outStr)

@client.event
async def on_raw_reaction_add(payload):
	log("reaction event:", True)
	log(f"    guild_id: {payload.guild_id}", True)
	log(f"    emoji.name: {payload.emoji.name.encode('unicode_escape').decode('ascii')}", True) 
	if payload.guild_id == 867607664837132298 and payload.emoji.name == "\u2665\ufe0f":
		message = await client.get_channel(payload.channel_id).fetch_message(payload.message_id)
		await message.clear_reaction("\u2665\ufe0f")

async def say(channel_name, msg, message, count = 1):
	if not (message.attachments is None or message.attachments == []):
		urls = []
		for f in message.attachments:
			urls.append(f.url)
		msg = "{} [ {} ]".format(msg, ", ".join(urls))
			
	channel = None
	for i in message.guild.text_channels:
		if channel_name in [i.name, str(i.id), f"<#{i.id}>"]:
			channel = i
	
	fails = 0
	for i in range(count):
		if channel is None:
			await send(message.channel, "error: channel not found.")
			return
		else:
			try:
				await send(channel, msg)
			except:
				await send(message.channel, f"error: failed to send message to <#{channel.id}>.")
				fails += 1
				if fails >= 5:
					return
			else:
				await message.add_reaction("\u2705")
		await asyncio.sleep(1)

async def pronoun_command(message, parts):
	global pronoun_db
	presets = {
		"they": ["they", "them", "their", "theirs", "themself"],
		"she": ["she", "her", "her", "hers", "herself"],
		"he": ["he", "him", "his", "his", "himself"],
		"it": ["it", "it", "its", "its", "itself"]
	}

	if len(parts) > 1:
		#add pronouns
		if parts[1] == "add":
			if len(parts) == 1:
				await send(message.channel, "error: not enough arguments.")
				return
			if parts[2] in presets:
				pronoun = presets[parts[2]]
			else:
				pronoun = parts[2].split("/")
				if len(pronoun) != 5:
					await send(message.channel, "error: invalid syntax.")
					return
			if pronoun_db.get(message.author.id) is None:
				pronoun_db[message.author.id] = [pronoun]
			else:
				pronoun_db[message.author.id].append(pronoun)
			write_pronoun_db()
			await send(message.channel, f"added pronoun {'/'.join(pronoun)}.")
			return

		#clear pronouns
		elif parts[1] == "clear":
			if message.author.id in pronoun_db:
				pronoun_db.pop(message.author.id)
				write_pronoun_db()
				await send(message.channel, "cleared pronouns.")
			else:
				await send(message.channel, "no pronouns to clear.")
			return

		#list pronouns
		elif parts[1] == "list":
			if message.author.id in pronoun_db:
				await send(message.channel, "\n".join(["/".join(pn) for pn in pronoun_db[message.author.id]]))
			else:
				await send(message.channel, "no pronouns to list.")
			return
	
	await send(message.channel, "error: first argument must be \"add\", \"clear\", or \"list\".")
			
def write_pronoun_db():
	with open("pronoun_db", "w") as f:
		for user in pronoun_db:
			f.write(str(user) + "=" + ",".join(["/".join(pn) for pn in pronoun_db[user]]) + "\n")
		

def pronounize(text, user_id = None, pronoun = None):
	if not pronoun:
		pronoun = ["they", "them", "their", "theirs", "themself"]
		if user_id == 0:
			pronoun[4] = "themselves"
		elif user_id in pronoun_db:
			pronoun = random.choice(pronoun_db[user_id])
	
	text = re.sub(r"(?<!\\)\$themself", pronoun[4], text)
	text = re.sub(r"(?<!\\)\$theirs", pronoun[3], text)
	text = re.sub(r"(?<!\\)\$their", pronoun[2], text)
	text = re.sub(r"(?<!\\)\$them", pronoun[1], text)
	text = re.sub(r"(?<!\\)\$they", pronoun[0], text)
	text = re.sub(r"(?<!\\)\$Themself", pronoun[4].capitalize(), text)
	text = re.sub(r"(?<!\\)\$Theirs", pronoun[3].capitalize(), text)
	text = re.sub(r"(?<!\\)\$Their", pronoun[2].capitalize(), text)
	text = re.sub(r"(?<!\\)\$Them", pronoun[1].capitalize(), text)
	text = re.sub(r"(?<!\\)\$They", pronoun[0].capitalize(), text)
	text = re.sub(r"\\\$", "$", text)

	return text

client.run(token)
